package com.rpoplavskij.board.repository;

import com.rpoplavskij.board.domain.Message;
import org.springframework.data.repository.CrudRepository;


public interface MessageRepository extends CrudRepository<Message, Long> {

}
